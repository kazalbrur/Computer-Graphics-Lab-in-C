
## Computer Graphics Lab Programs:

1. Liang-Barsky Line Clipping Algorithm with Window to viewport Mapping

2. Cohen-Suderland Line Clipping Algorithm with Window to viewport Mapping

3. Cylinder and Parallelepiped by extruding Circle and Quadrilateral.

4. Simple shaded scene consisting of a tea pot on a table

5. Rotating cube with viewer movement

6. Scan-Line algorithm for filling a polygon

7. Write a program to create a chess board using DDA line algorithm.

8. Write a Program to implement Bresenham’s line drawing algorithm with all values of slopes.

9. Write a Program to implement mid point circle generation algorithm.

10. Write a Program to create a wire frame model of globe using equation of ellipse.

11. Write a program to create and fill two dimensional object by using boundaryfill algorithm.

12. Write a program to create (without using built in function) a cube by implementing translation algorithm by translating along
	 (i) X-axis 
	 (ii) Y- axis and 
	 (iii) XY plane.

13. Write a Program to create (without using built-in function) and rotate 
	1. given angle, 
	2. around X and Y axis a triangle by implementing rotation algorithm.

14. Write a Program to create (without using built-in function) a triangle by implementing scaling algorithm by zooming/un-zooming along 
	(i) X axis 
	(ii) Y axis 
	(iii)XY plane.

15. Write a program to create (without using built-in function) a cube and implement reflection algorithm 
	(i) X axis 
	(ii) Y axis

16. Write a program to create (without using built-in function) a square and implement shear algorithm along 
	(i) X axis 
	(ii) Y axis
 
17. Write a program to display a bar graph using the poly-line function. Input to the program is to include the data points and the labeling required for the x and y axes. The data points are to be scaled by the program so that the graph is displayed across the full screen area.

18. Write a program to display a bar graph in any selected scene area. Use the poly-line function to draw the bars.

19. Write a procedure to display a line graph for any input set of data points in any selected area of the screen, with the input dam set scaled to fit the selected screen area. Data points are to be displayed as asterisks joined with straight line segments, and the x and y axes are to be labeled according to input specifications. (Instead of asterisks small circles or some other symbols could be used to plot the data points.)

20. Using the circle function, write a routine to display a pie chart with appropriate labeling. Input to the routine is to include a data set giving the distribution of the data over some set of intervals, the name of the pie chart, and the names of the intervals. Each section label IS to be displayed outside the boundary of the pie chart near the corresponding pie section.



## Environment Installation: 

### For Linux

Install the packages codeblocks, freeglut3, freeglut3-dev & binutils-gold using the following command. 

	$ sudo add-apt-repository ppa:damien-moore/codeblocks-stable
	$ sudo apt update
	$ sudo apt install codeblocks
	$ sudo apt-get install freeglut3
	$ sudo apt-get install freeglut3-dev
	$ sudo apt-get install binutils-gold
