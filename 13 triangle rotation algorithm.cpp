/*
Kazal Chandra Barman 
4th Year 1st Semester 
Begum Rokeya University, Rangpur.
 
*/


#include <windows.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#define PI 3.142
using namespace std;

double p[3][2],theta, x, y;
int choice,ch;


void Init()
{
    glClearColor(0.0,1.0,1.0,0);     // Set background color to black and opaque
    glClear(GL_COLOR_BUFFER_BIT);    // Clear the color buffer (background)

    glColor3f(0.0,0.0,1.0);

    glViewport(0 , 0 , 640 , 480);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0 , 2000 , 0 , 1000);
}
void RotateTriangle(int angle, double xr,double yr)
{
     Init();
     glBegin(GL_TRIANGLES);
        glVertex2dv(p[0]);
        glVertex2dv(p[1]);
        glVertex2dv(p[2]);
    glEnd();

    glFlush();

    for(int i=0;i<3;i++)
    {
        double a = xr + ((p[i][0] - xr) * cos(theta*(PI/180)) - (p[i][1]-yr) * sin(theta*(PI/180)));
        double b = yr + ((p[i][0] - xr) * sin(theta*(PI/180)) + (p[i][1]-yr) * cos(theta*(PI/180)));
        cout<<"COMPUTER NEW COORDINATE VALUES ( "<<a<<"\t"<<b <<")";
        p[i][0] = a ; p[i][1] = b;
    }

    glBegin(GL_TRIANGLES);
        glVertex2dv(p[0]);
        glVertex2dv(p[1]);
        glVertex2dv(p[2]);
    glEnd();
    glFlush();
}

void Rotate() {

    Init();
    glBegin(GL_TRIANGLES);
        glVertex2dv(p[0]);
        glVertex2dv(p[1]);
        glVertex2dv(p[2]);
    glEnd();

    glFlush();

    cout<<"\n Rotate.......";
    cout<<"\n1.with given angle";

    cout<<"\n2. X and Y AXIS";


    cout <<"\n3. Exit";
    cout<<"\nEnter your choice :";
    cin>>choice;
    switch(choice)
    {
     case 1:  cout<<"\nEnter angle";
              cin>>theta;
              RotateTriangle(theta,0,0);
              break;
     case 2: cout<<"Enter theta, x and y value";
             cin>>theta;
             cin>>x>>y;
             RotateTriangle(theta,x,y);
             break;
    case 3: exit(0);
    default:cout<<"Invalid choice";
               break;
    }

}





/* Main function: GLUT runs as a console application starting at main()  */
int main(int argc, char** argv) {
    cout<<"Enter Three Points for Drawing a Triangle:\n";
    cout<<"\nEnter Point1( X1 , Y1):";
    cin>>p[0][0]>>p[0][1];
    cout<<"\nEnter Point2( X2 , Y2):";
    cin>>p[1][0]>>p[1][1];
    cout<<"\nEnter Point3( X3 , Y3):";
    cin>>p[2][0]>>p[2][1];
    cout << "\nPress any key to continue...";
    cin>>ch;
   glutInit(&argc, argv);                 // Initialize GLUT
   glutCreateWindow("OpenGL program to Rotate a Triangle"); // Create a window with the given title
   glutInitWindowSize(320, 320);   // Set the window's initial width & height
   glutInitWindowPosition(50, 50); // Position the window's initial top-left corner
   glutDisplayFunc(Rotate); // Register display callback handler for window re-paint
   glutMainLoop();           // Enter the event-processing loop
   return 0;
}