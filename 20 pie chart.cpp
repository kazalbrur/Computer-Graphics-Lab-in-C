/*
Kazal Chandra Barman 
4th Year 1st Semester 
Begum Rokeya University, Rangpur.
 
*/


#include <stdio.h>
#include <graphics.h>

int main()
{
    int gd=DETECT,gm;
	initgraph(&gd,&gm,NULL);
   int  x, y;

   //settextstyle(BOLD_FONT,HORIZ_DIR,2);
   outtextxy(220,10,"PIE CHART");
   /* Setting cordinate of center of circle */
   x = getmaxx()/2;
   y = getmaxy()/2;

   setcolor(RED);
   pieslice(x, y, 0, 60, 120);
   outtextxy(x + 140, y - 70, "FOOD");

   setcolor(BLUE);
   pieslice(x, y, 60, 160, 120);
   outtextxy(x - 30, y - 170, "RENT");

 //  setfillstyle(SOLID_FILL, GREEN);
   pieslice(x, y, 160, 220, 120);
   outtextxy(x - 250, y, "ELECTRICITY");

   setcolor(GREEN);
   pieslice(x, y, 220, 360, 120);
   outtextxy(x, y + 150, "SAVINGS");

    getch();
	delay(5000);
	closegraph();
	return 0;
}

