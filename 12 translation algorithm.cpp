// cube translation

#include <GL/glut.h>
#include <math.h>
#include<stdio.h>
//int p[8][2] = {{50,50},{50,100},{100,100},{100,50},{120,70},{120,120},{70,70},{70,120}};
int p[8][2] = {{100,100},{100,200},{200,100},{200,200},{250,150},{250,250},{150,150},{150,250}};
int choice, tx,ty;
void Init()
{
    glClearColor(0.0,0.0,0.0,0.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1.0,0.0,1.0);
    glViewport(0 , 0 , 640, 480);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0 , 2000, 0 , 1000);
}
void CUBETranslation(int tx, int ty) {
    Init();
    for(int i=0; i<8; i++)
    {
       p[i][0] = p[i][0] + tx;
        p[i][1] = p[i][1] + ty;
    }
    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    glBegin(GL_QUAD_STRIP);
        glVertex2iv(p[0]);
        glVertex2iv(p[1]);
        glVertex2iv(p[2]);
        glVertex2iv(p[3]);
        glVertex2iv(p[4]);
       glVertex2iv(p[5]);
       glVertex2iv(p[6]);
        glVertex2iv(p[7]);
       glVertex2iv(p[0]);
       glVertex2iv(p[1]);
    glEnd();
   glFlush();  // Render now
}

void Translation() {
    Init();
    CUBETranslation(0,0);
    for(;;)
    {
    printf("\n Translation \n1.X-AXIS \n2.Y_AXIS \n3.XY PLANE \n4. Exit");
 
    printf("\nEnter your choice :");
 scanf("%d", &choice);
    switch(choice)
    {
     case 1:  printf("Enter tx value");
       scanf("%d", &tx);
              CUBETranslation(tx,0);
              break;
     case 2: printf("Enter ty value");
                scanf("%d", &ty);
               CUBETranslation(0,ty);
               break;
     case 3: printf("Enter tx and ty value");
               scanf("%d%d", &tx,&ty);
               CUBETranslation(tx,ty);
                break;
case 4: exit(0);
default:printf("Invalid choice");
               break;
 }
    }
 }
int main(int argc, char** argv) {

   printf("\nTranslation of Cube\n\n..........Demo.....\n ");
   glutInit(&argc, argv);                 // Initialize GLUT
   glutCreateWindow("OpenGL Translation "); // Create a window with the given title
   glutInitWindowSize(640, 480);   // Set the window's initial width & height
   glutInitWindowPosition(50, 50); // Position the window's initial top-left corner
   glutDisplayFunc(Translation); // Register display callback handler for window re-paint
   glutMainLoop();           // Enter the event-processing loop
   return 0;
}