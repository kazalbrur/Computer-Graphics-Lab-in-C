/*
Kazal Chandra Barman 
4th Year 1st Semester 
Begum Rokeya University, Rangpur.
 
*/

// Reflexion of  Cube

#include <GL/glut.h>
#include <stdio.h>
#include <math.h>
int p[8][2] = {{100,100},{100,200},{200,100},{200,200},{250,150},{250,250},{150,150},{150,250}};
int choice, tx,ty;
int xreflex =300, yreflex=400;
void Init()
{
    glClearColor(0.0,0.0,0.0,0);
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1.0,1.0,1.0);
    glViewport(0 , 0 , 640, 480);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0 , 2000, 0 , 1000);
}
void CUBEReflex(int rx, int ry) {
    for(int i=0; i<8; i++)
    {
       p[i][0] =  p[i][0] + rx;
        p[i][1] = p[i][1] + ry;
    }
    glBegin(GL_LINES);
        glVertex2i(400,0);
        glVertex2i(400,1000);
        glVertex2i(0,300);
        glVertex2i(2000,300);
    glEnd();
 glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    glBegin(GL_QUAD_STRIP);
        glVertex2iv(p[0]);
        glVertex2iv(p[1]);
        glVertex2iv(p[2]);
        glVertex2iv(p[3]);
       glVertex2iv(p[4]);
       glVertex2iv(p[5]);
        glVertex2iv(p[6]);
        glVertex2iv(p[7]);
       glVertex2iv(p[0]);
       glVertex2iv(p[1]);
    glEnd();

   glFlush();  // Render now
}
void Reflex() {
    Init();
    for(int k=0; k<8; k++)
    {
        p[k][0] +=400; p[k][1] +=300;
    }
    CUBEReflex(1,1);
 for(;;)
  {
 printf("\n Reflection\n1.X-AXIS\n2.Y_AXIS\n3. Exit");
 printf("\nEnter your choice :");
 scanf("%d",&choice);
 switch(choice)
    {
     case 1:Init();
     xreflex = -1*xreflex;
   CUBEReflex(0,xreflex);
              break;
     case  2: Init();
               yreflex = -1*yreflex;
              CUBEReflex(yreflex,0);
               break;
    case 3: exit(0);
    default:printf("Invalid choice");
               break;
    }
}
 }
int main(int argc, char** argv) {
   printf("\nReflexion of  Cube\n\n..........Demo.....\n ");
   glutInit(&argc, argv);                 // Initialize GLUT
   glutCreateWindow("OpenGL Translation "); // Create a window with the given title
   glutInitWindowSize(640, 480);   // Set the window's initial width & height
   glutInitWindowPosition(50, 50); // Position the window's initial top-left corner

   glutDisplayFunc(Reflex); // Register display callback handler for window re-paint
   glutMainLoop();           // Enter the event-processing loop
   return 0;
}
